 /*
rpn-calc - Shell based RPN calculator
Copyright (C) 2020  Tim Cassels

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "rpn-calc.h"


void add()
{
    x = x + y;
    y = z;
    z = t;
    t = 0;
}

void subtract()
{
    x = y - x;
    y = z;
    z = t;
    t = 0;
}

void multiply()
{
    x = x * y;
    y = z;
    z = t;
    t = 0;
}

void divide()
{
    x = y / x;
    y = z;
    z = t;
    t = 0;
}