 /*
rpn-calc - Shell based RPN calculator
Copyright (C) 2020  Tim Cassels

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "functions.h"
#include "basic.h"
#include "trig.h"


/* These for variables act as the RPN registers */
double t = 0;
double z = 0;
double y = 0;
double x = 0;


/* This is the user input temp store */
char input[500];


/* Writes the four varable registers to screen */
void display()
{
    system("clear");
    printf("T: %f\n", t);
    printf("Z: %f\n", z);
    printf("Y: %f\n", y);
    printf("X: %f\n", x);
}


void read_user_input()
{
    fgets(input, 500, stdin);
    int i;
    int len = sizeof(input);
    for (i = 0;i <= len; i++) {
        if(input[i] == '\n') {
            input[i] = '\0';
        }
    }
    printf("\n::%s::\n", input);
}

void insert_x()
{
    t = z;
    z = y;
    y = x;
    x = atoi(input);
}




int main()
{
    printf("HomeBrew RPN Calc - GetFucked\n");

    int program_running = 1;

    while(program_running == 1) {
        display();
        read_user_input();

        if (strcmp(input, "add") == 0) {
            add();
        } else if (strcmp(input, "sub") == 0) {
            subtract();
        } else if (strcmp(input, "mul") == 0) {
            multiply();
        } else if (strcmp(input, "div") == 0) {
            divide();
        } else if (strcmp(input, "sin") == 0) {
            sine();
        } else if (strcmp(input, "cos") == 0) {
            cosine();
        } else if (strcmp(input, "tan") == 0) {
            tangent();
        } else if (strcmp(input, "xy") == 0) {
            xy();
        } else {
            insert_x();
        }
    }
    
}