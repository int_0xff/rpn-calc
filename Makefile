CC = gcc
CFLAGS =

SOURCE = $(wildcard src/*.c)
OBJS = $(SOURCE:.c=.o)


rpn_calc: ${OBJS}
	${CC} -ansi -s -O3 -Wall -pedantic ${OBJS} -o bin/rpn-calc -lm



clean:
	rm -rf bin/rpn-calc
	rm -rf src/*.o


ctags:
	ctags src/*.c src/*.h